const tf = require("@tensorflow/tfjs-node");
const cocoSsd = require("@tensorflow-models/coco-ssd");
const Jimp = require('jimp');

let model;
async function runDetection() {
  model = await cocoSsd.load();
  Jimp.read('./carro.jpg', async(err, image) => {
    if (err) throw err;
    
    const data = image.bitmap.data;
    const width = image.bitmap.width;
    const height = image.bitmap.height;
    
    const input =  await model.detect({data, width, height});
    console.log(input[0].class, input[0].score);
  });

  // const video = document.createElement('video');
  // const canvas = document.createElement('canvas');
  // const ctx = canvas.getContext('2d');
  // document.body.appendChild(video);
  // document.body.appendChild(canvas);

  // const constraints = {
  //   video: {
  //     facingMode: 'environment'
  //   }
  // };

  // const stream = await navigator.mediaDevices.getUserMedia(constraints);
  // video.srcObject = stream;
  // video.width = 640;
  // video.height = 480;
  // video.addEventListener('playing', () => {
  //   canvas.width = video.videoWidth;
  //   canvas.height = video.videoHeight;
  //   setInterval(async () => {
  //     ctx.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
  //     const input = tf.browser.fromPixels(canvas);
  //     const predictions = await model.detect(input);
  //     console.log(predictions);
  //     // Here you can do additional processing of the predictions, such as filtering
  //     // for cars only and drawing bounding boxes on the video stream.
  //     input.dispose();
  //   }, 1000 / 30);
  // });
}
setInterval(async() => {
  await runDetection();
}, 1000);

