const cv = require("opencv4nodejs");
const stream = new cv.VideoCapture(
  `rtsp://${"DNA"}:${"DNA2023!"}@${"98.173.8.28:5554"}/cam/realmonitor?channel=1&subtype=0&unicast=true&proto=Onvif`
);

let frame = new cv.Mat();
let previous = new cv.Mat();
stream.read(frame);
frame.copyTo(previous);

const delay = 30;
let framesPassed = 0;

while (true) {
  const success = stream.read(frame);
  if (!success) break;

  const difference = frame.absdiff(previous);

  const gray = difference.cvtColor(cv.COLOR_RGBA2GRAY);
  const thresholded = gray.threshold(25, 255, cv.THRESH_BINARY);

  const contours = thresholded.findContours(
    cv.RETR_EXTERNAL,
    cv.CHAIN_APPROX_SIMPLE
  );
  if (contours.size() > 0) {
    // Enviar la imagen actual a la API de detección de placas
    console.log("envias la imagen");
  }

  frame.copyTo(previous);
  framesPassed++;

  if (framesPassed % delay === 0) {
    console.log("No movement detected.");
  }

  cv.imshow("frame", frame);
  cv.imshow("difference", difference);

  const key = cv.waitKey(1);
  if (key === 27) break;
}

stream.release();
cv.destroyAllWindows();
